<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('', 'HomeController@index')->name('home');
Route::get('/pcu', 'HomeController@index')->middleware('auth');

Route::get('/clear', 'ClearController@index');

Route::get('/islogged', 'AuthController@checkLoggin');


Auth::routes();

Route::get('/auth/steam', 'AuthController@redirectToSteam')->name('auth.steam');
Route::get('/auth/steam/handle', 'AuthController@handle')->name('auth.steam.handle');
Route::get('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/api/user', 'UserController@index');
