<?php

namespace App\Http\Controllers;

use Invisnik\LaravelSteamAuth\SteamAuth;
use App\User;
use Auth;

class AuthController extends Controller
{
    /**
     * The SteamAuth instance.
     *
     * @var SteamAuth
     */
    protected $steam;

    /**
     * The redirect URL.
     *
     * @var string
     */
    protected $redirectURL = '/pcu';

    /**
     * AuthController constructor.
     *
     * @param SteamAuth $steam
     */
    public function __construct(SteamAuth $steam)
    {
        $this->steam = $steam;
    }

    /**
     * Redirect the user to the authentication page
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function redirectToSteam()
    {
        return $this->steam->redirect();
    }

    /**
     * Get user info and log in
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function handle()
    {
        if ($this->steam->validate()) {
            $info = $this->steam->getUserInfo();

            if (!is_null($info)) {
                $user = $this->findOrNewUser($info);
                if(!is_null($user)) {
                    Auth::login($user, true);

                    return redirect($this->redirectURL); // redirect to site
                } else {
                    return "Tu cuenta no exite, registrate en el servidor.";
                }
            }
        }
        return $this->redirectToSteam();
    }

    /**
     * Getting user by info or created if not exists
     *
     * @param $info
     * @return User
     */
    protected function findOrNewUser($info)
    {
        $steamID = "steam:".dechex($info->steamID64);
        $user = User::where('steamid', $steamID)->first();

        if (!is_null($user)) {
            if(is_null($user->avatar)) {
                $user->avatar = $info->avatarfull;
                $user->save();
            }
            return $user;
        } else {
            return null;
        }

        /*return User::create([
            'username' => $info->personaname,
            'avatar' => $info->avatarfull,
            'steamid' => $steamID
        ]);*/
    }
}
