import Home from './views/Home';
import PCU  from './views/PCU';

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/groxor',
            name: 'home',
            component: Home
        },
        {
            path: '/groxor/pcu',
            name: 'pcu',
            component: PCU
        }
    ]
});

export default router;
