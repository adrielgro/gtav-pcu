//import Vue from 'vue'
//import Vuetify from 'vuetify/lib'
//import 'vuetify/dist/vuetify.min.css'
import App      from './App.vue'
import routes   from './routes.js'
//import store    from './store/'
//import Vuex     from 'vuex'

// Cargar modulos
//Vue.use(Vuex)
Vue.use(Vuetify)

// Comunicador de componentes
//window.EventBus = new Vue();

// Componentes
//Vue.component('LoadingScreen',  require('./components/LoadingScreen.vue').default);
Vue.component('gta-navbar',       require('./components/NavbarComponent.vue').default);
//Vue.component('c-footer',       require('../shopyhand/components/FooterComponent.vue').default);

const root_element = document.getElementById('app');

new Vue({
    el: root_element,
    //store,
    router: routes,
    vuetify: new Vuetify(),
    render: h => h(App, {
        props: {
            user_name: root_element.getAttribute('user-name'),
            user_auth: root_element.getAttribute('user-auth')
        }
    })
})
