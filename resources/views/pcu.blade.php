<!DOCTYPE html>
<html>
    <head>
        <link href="https://fonts.googleapis.com/css?family=Material+Icons|Roboto:100,300,400,500,700,900|Lato:400,700,900|Montserrat:300,400,500,600,700|Raleway:100,600" rel="stylesheet">
        <link href="https://cdn.jsdelivr.net/npm/@mdi/font@4.x/css/materialdesignicons.min.css" rel="stylesheet">
        <link href="https://cdn.jsdelivr.net/npm/vuetify@2.x/dist/vuetify.min.css" rel="stylesheet">
        <link href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" rel="stylesheet">
        <link href="css/master.css" rel="stylesheet">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, minimal-ui">
    </head>
    <body>
        <div id="app">
            <v-app>
                <v-toolbar flat light color="white" class="px-10">

                    <v-toolbar-items>
                        <v-btn text href="/">Inicio</v-btn>
                        <v-btn text>Foro</v-btn>
                        <v-btn text href="/pcu">PCU</v-btn>
                        <v-btn text>Servidores</v-btn>
                        <v-btn text>Donaciones</v-btn>
                    </v-toolbar-items>

                    <v-spacer></v-spacer>

                    <template>
                        <v-menu offset-y>
                            <template v-slot:activator="{ on }">
                                <v-btn text class="ma-2" v-on="on">Groxor
                                    <v-icon right dark>keyboard_arrow_down</v-icon>
                                </v-btn>
                            </template>
                            <v-list dense>
                                <v-list-item @click="">
                                    <v-list-item-title>Perfil</v-list-item-title>
                                </v-list-item>
                                <v-list-item @click="">
                                    <v-list-item-title>Salir</v-list-item-title>
                                </v-list-item>
                            </v-list>
                        </v-menu>
                    
                        <v-btn icon href="https://www.gtav.es/estado" target="_blank">
                            <v-icon>fas fa-server</v-icon>
                        </v-btn>
                    </template>


                    <template v-slot:extension>
                        <v-tabs v-model="tab" align-with-title background-color="transparent">
                            <v-tab key="info">Información</v-tab>
                            <v-tab key="inventory">Inventario</v-tab>
                            <v-tab key="vehicles">Vehículos</v-tab>
                            <v-tab key="properties">Propiedades</v-tab>
                            <v-tab key="logs">Logs</v-tab>
                        </v-tabs>
                        </template>

                </v-toolbar>
                <v-content>
                    
                    

                    <v-footer padless color="grey lighten-2 py-4">
                        <v-col class="text-center" cols="12">
                            2019 — <strong>GTA V</strong>
                        </v-col>
                    </v-footer>

                </v-content>
            </v-app>
        </div>

        <style>
            .v-application.theme--light {
                background-image: url('/static/imgs/backgrounds/leaves-pattern.png');
            }
        </style>

        <script src="https://cdn.jsdelivr.net/npm/vue@2.x/dist/vue.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/vuetify@2.x/dist/vuetify.js"></script>
        <script>
        new Vue({
            el: '#app',
            vuetify: new Vuetify(),
            data: {
                tab: null
            }
        })
        </script>
    </body>
</html>